alias ls='ls --color=auto'
alias la='ls -a'
alias ll='ls -la'
alias l='ls'
alias l.="ls -A | egrep '^\.'"
alias hscd="cd ~/%f"
#alias news="Exec=env temp='initial value' vared  shit && curl getnews.tech/$shit"
alias cd..='cd ..'
alias pdw="pwd"
alias udpate='sudo pacman -Syyu'
alias upate='sudo pacman -Syyu'
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias df='df -hx "squashfs"'
alias unlock="sudo rm /var/lib/pacman/db.lck"
#alias radd="read -p packages &&  trizen -S -a --noinstall --movepkg-dir=~/Documents/.stuff/pazza-repo3/repo $packages && cd $stuff && repoctl update && bash ../git.sh"
alias free="free -mt"
alias uac="sh ~/.bin/main/000*"
alias wget="wget -c"
alias userlist="cut -d: -f1 /etc/passwd"
# Aliases for software managment
alias pacman='sudo pacman --color auto'
alias update='sudo pacman -Syyu'
alias pksyua="yay -Syu --noconfirm"
alias upall="yay -Syu --noconfirm"
alias systemctl="sudo systemctl --no-legend"
alias psa="ps auxf"
alias psgrep="ps aux | grep -v grep | grep -i -e VSZ -e"
alias update-grub="sudo grub-mkconfig -o /boot/grub/grub.cfg"
alias update-fc='sudo fc-cache -fv'
alias skel='cp -Rf ~/.config ~/.config-backup-$(date +%Y.%m.%d-%H.%M.%S) && cp -rf /etc/skel/* ~'
alias bupskel='cp -Rf /etc/skel ~/.skel-backup-$(date +%Y.%m.%d-%H.%M.%S)'
#alias cb='sudo cp /etc/skel/.bashrc ~/.bashrc && source ~/.bashrc'
alias cz='sudo cp /etc/skel/.zshrc ~/.zshrc && source ~/.zshrc'
alias tobash="sudo chsh $USER -s /bin/bash && echo 'Now log out.'"
alias tozsh="sudo chsh $USER -s /bin/zsh && echo 'Now log out.'"
alias kc='killall conky'
alias hw="hwinfo --short"
alias yayskip='yay -S --mflags --skipinteg'
alias trizenskip='trizen -S --skipinteg'
alias docker="sudo docker"
alias microcode='grep . /sys/devices/system/cpu/vulnerabilities/*'
alias mirror="sudo reflector -f 30 -l 30 --number 10 --verbose --save /etc/pacman.d/mirrorlist"
alias mirrord="sudo reflector --latest 50 --number 20 --sort delay --save /etc/pacman.d/mirrorlist"
alias mirrors="sudo reflector --latest 50 --number 20 --sort score --save /etc/pacman.d/mirrorlist"
alias mirrora="sudo reflector --latest 50 --number 20 --sort age --save /etc/pacman.d/mirrorlist"
alias vbm="sudo mount -t vboxsf -o rw,uid=1000,gid=1000 Public /home/$USER/Public"
#shopt -s expand_aliases # expand aliases
alias yta-aac="youtube-dl --extract-audio --audio-format aac "
alias yta-best="youtube-dl --extract-audio --audio-format best "
alias yta-flac="youtube-dl --extract-audio --audio-format flac "
alias yta-m4a="youtube-dl --extract-audio --audio-format m4a "
alias yta-mp3="youtube-dl --extract-audio --audio-format mp3 "
alias yta-opus="youtube-dl --extract-audio --audio-format opus "
alias yta-wav="youtube-dl --extract-audio --audio-format wav "
alias cleanup='sudo pacman -Rns $(pacman -Qtdq)'
alias nlightdm="sudo nano /etc/lightdm/lightdm.conf"
alias npacman="sudo nano /etc/pacman.conf"
alias ngrub="sudo nano /etc/default/grub"
alias nmkinitcpio="sudo nano /etc/mkinitcpio.conf"
alias nslim="sudo nano /etc/slim.conf"
alias noblogout="sudo nano /etc/oblogout.conf"
alias nmirrorlist="sudo nano /etc/pacman.d/mirrorlist"
alias nconfgrub="sudo nano /boot/grub/grub.cfg"
alias gpg-check="gpg2 --keyserver-options auto-key-retrieve --verify"
alias gpg-retrieve="gpg2 --keyserver-options auto-key-retrieve --receive-keys"
alias ssn="sudo shutdown now"
alias sr="sudo reboot"
#create a file called .zshrc-personal and put all your personal aliases
alias ls='ls --color=auto'
alias la='ls -a'
alias ll='ls -la'
alias l='ls'
alias l.="ls -A | egrep '^\.'"
alias hscd="cd ~/%f"
#alias news="Exec=env temp='initial value' vared  shit && curl getnews.tech/$shit"
alias cd..='cd ..'
alias pdw="pwd"
alias udpate='sudo pacman -Syyu'
alias upate='sudo pacman -Syyu'
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias df='df -hx "squashfs"'
alias unlock="sudo rm /var/lib/pacman/db.lck"
#alias radd="read -p packages &&  trizen -S -a --noinstall --movepkg-dir=~/Documents/.stuff/pazza-repo3/repo $packages && cd $stuff && repoctl update && bash ../git.sh"
alias free="free -mt"
alias uac="sh ~/.bin/main/000*"
alias wget="wget -c"
alias userlist="cut -d: -f1 /etc/passwd"
# Aliases for software managment
alias pacman='sudo pacman --color auto'
alias update='sudo pacman -Syyu'
alias pksyua="yay -Syu --noconfirm"
alias upall="yay -Syu --noconfirm"
alias systemctl="sudo systemctl --no-legend"
alias psa="ps auxf"
alias psgrep="ps aux | grep -v grep | grep -i -e VSZ -e"
alias update-grub="sudo grub-mkconfig -o /boot/grub/grub.cfg"
alias update-fc='sudo fc-cache -fv'
alias skel='cp -Rf ~/.config ~/.config-backup-$(date +%Y.%m.%d-%H.%M.%S) && cp -rf /etc/skel/* ~'
alias bupskel='cp -Rf /etc/skel ~/.skel-backup-$(date +%Y.%m.%d-%H.%M.%S)'
#alias cb='sudo cp /etc/skel/.bashrc ~/.bashrc && source ~/.bashrc'
alias cz='sudo cp /etc/skel/.zshrc ~/.zshrc && source ~/.zshrc'
alias tobash="sudo chsh $USER -s /bin/bash && echo 'Now log out.'"
alias tozsh="sudo chsh $USER -s /bin/zsh && echo 'Now log out.'"
alias kc='killall conky'
alias hw="hwinfo --short"
alias yayskip='yay -S --mflags --skipinteg'
alias trizenskip='trizen -S --skipinteg'
alias docker="sudo docker"
alias microcode='grep . /sys/devices/system/cpu/vulnerabilities/*'
alias mirror="sudo reflector -f 30 -l 30 --number 10 --verbose --save /etc/pacman.d/mirrorlist"
alias mirrord="sudo reflector --latest 50 --number 20 --sort delay --save /etc/pacman.d/mirrorlist"
alias mirrors="sudo reflector --latest 50 --number 20 --sort score --save /etc/pacman.d/mirrorlist"
alias mirrora="sudo reflector --latest 50 --number 20 --sort age --save /etc/pacman.d/mirrorlist"
alias vbm="sudo mount -t vboxsf -o rw,uid=1000,gid=1000 Public /home/$USER/Public"
#shopt -s expand_aliases # expand aliases
alias yta-aac="youtube-dl --extract-audio --audio-format aac "
alias yta-best="youtube-dl --extract-audio --audio-format best "
alias yta-flac="youtube-dl --extract-audio --audio-format flac "
alias yta-m4a="youtube-dl --extract-audio --audio-format m4a "
alias yta-mp3="youtube-dl --extract-audio --audio-format mp3 "
alias yta-opus="youtube-dl --extract-audio --audio-format opus "
alias yta-wav="youtube-dl --extract-audio --audio-format wav "
alias cleanup='sudo pacman -Rns $(pacman -Qtdq)'
alias nlightdm="sudo nano /etc/lightdm/lightdm.conf"
alias npacman="sudo nano /etc/pacman.conf"
alias ngrub="sudo nano /etc/default/grub"
alias nmkinitcpio="sudo nano /etc/mkinitcpio.conf"
alias nslim="sudo nano /etc/slim.conf"
alias noblogout="sudo nano /etc/oblogout.conf"
alias nmirrorlist="sudo nano /etc/pacman.d/mirrorlist"
alias nconfgrub="sudo nano /boot/grub/grub.cfg"
alias gpg-check="gpg2 --keyserver-options auto-key-retrieve --verify"
alias gpg-retrieve="gpg2 --keyserver-options auto-key-retrieve --receive-keys"
alias ssn="sudo shutdown now"
alias sr="sudo reboot"
#create a file called .zshrc-personal and put all your personal aliases
alias ls='ls --color=auto'
alias la='ls -a'
alias ll='ls -la'
alias l='ls'
alias l.="ls -A | egrep '^\.'"
alias hscd="cd ~/%f"
#alias news="Exec=env temp='initial value' vared  shit && curl getnews.tech/$shit"
alias cd..='cd ..'
alias pdw="pwd"
alias udpate='sudo pacman -Syyu'
alias upate='sudo pacman -Syyu'
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias df='df -hx "squashfs"'
alias unlock="sudo rm /var/lib/pacman/db.lck"
#alias radd="read -p packages &&  trizen -S -a --noinstall --movepkg-dir=~/Documents/.stuff/pazza-repo3/repo $packages && cd $stuff && repoctl update && bash ../git.sh"
alias free="free -mt"
alias uac="sh ~/.bin/main/000*"
alias wget="wget -c"
alias userlist="cut -d: -f1 /etc/passwd"
# Aliases for software managment
alias pacman='sudo pacman --color auto'
alias update='sudo pacman -Syyu'
alias pksyua="yay -Syu --noconfirm"
alias upall="yay -Syu --noconfirm"
alias systemctl="sudo systemctl --no-legend"
alias psa="ps auxf"
alias psgrep="ps aux | grep -v grep | grep -i -e VSZ -e"
alias update-grub="sudo grub-mkconfig -o /boot/grub/grub.cfg"
alias update-fc='sudo fc-cache -fv'
alias skel='cp -Rf ~/.config ~/.config-backup-$(date +%Y.%m.%d-%H.%M.%S) && cp -rf /etc/skel/* ~'
alias bupskel='cp -Rf /etc/skel ~/.skel-backup-$(date +%Y.%m.%d-%H.%M.%S)'
#alias cb='sudo cp /etc/skel/.bashrc ~/.bashrc && source ~/.bashrc'
alias cz='sudo cp /etc/skel/.zshrc ~/.zshrc && source ~/.zshrc'
alias tobash="sudo chsh $USER -s /bin/bash && echo 'Now log out.'"
alias tozsh="sudo chsh $USER -s /bin/zsh && echo 'Now log out.'"
alias kc='killall conky'
alias hw="hwinfo --short"
alias yayskip='yay -S --mflags --skipinteg'
alias trizenskip='trizen -S --skipinteg'
alias docker="sudo docker"
alias microcode='grep . /sys/devices/system/cpu/vulnerabilities/*'
alias mirror="sudo reflector -f 30 -l 30 --number 10 --verbose --save /etc/pacman.d/mirrorlist"
alias mirrord="sudo reflector --latest 50 --number 20 --sort delay --save /etc/pacman.d/mirrorlist"
alias mirrors="sudo reflector --latest 50 --number 20 --sort score --save /etc/pacman.d/mirrorlist"
alias mirrora="sudo reflector --latest 50 --number 20 --sort age --save /etc/pacman.d/mirrorlist"
alias vbm="sudo mount -t vboxsf -o rw,uid=1000,gid=1000 Public /home/$USER/Public"
#shopt -s expand_aliases # expand aliases
alias yta-aac="youtube-dl --extract-audio --audio-format aac "
alias yta-best="youtube-dl --extract-audio --audio-format best "
alias yta-flac="youtube-dl --extract-audio --audio-format flac "
alias yta-m4a="youtube-dl --extract-audio --audio-format m4a "
alias yta-mp3="youtube-dl --extract-audio --audio-format mp3 "
alias yta-opus="youtube-dl --extract-audio --audio-format opus "
alias yta-wav="youtube-dl --extract-audio --audio-format wav "
alias cleanup='sudo pacman -Rns $(pacman -Qtdq)'
alias nlightdm="sudo nano /etc/lightdm/lightdm.conf"
alias npacman="sudo nano /etc/pacman.conf"
alias ngrub="sudo nano /etc/default/grub"
alias nmkinitcpio="sudo nano /etc/mkinitcpio.conf"
alias nslim="sudo nano /etc/slim.conf"
alias noblogout="sudo nano /etc/oblogout.conf"
alias nmirrorlist="sudo nano /etc/pacman.d/mirrorlist"
alias nconfgrub="sudo nano /boot/grub/grub.cfg"
alias gpg-check="gpg2 --keyserver-options auto-key-retrieve --verify"
alias gpg-retrieve="gpg2 --keyserver-options auto-key-retrieve --receive-keys"
alias ssn="sudo shutdown now"
alias sr="sudo reboot"
#create a file called .zshrc-personal and put all your personal aliases
